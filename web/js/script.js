(function($) {
    $(document).on('click', '.get-all-customers-in-json', function() {
        var self = $(this);

        $.get(
            $(this).data('url'),
            function( result ) {
                if (result.success) {
                    var data = [];
                    $.each( result.data, function(index, value) {
                        var row = '' +
                            '<td>' + value.name + '</td>' +
                            '<td>' + value.age + '</td>' +
                            '<td>' + value.email + '</td>' +
                            '<td>' + value.billing.address + '</td>' +
                            '<td>' + (value.shipping ? value.shipping.address : '') + '</td>';

                        data.push('<tr>' + row + '</tr>');
                    });

                    $('#customers-table').find('tbody').html(data.join(''));
                } else {
                    $('#customers-table').find('tbody').html('<tr></tr><td colspan="5">No result.</td></tr>');
                }
            },
            'json'
        );
    })
}(jQuery));