<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Address;
use AppBundle\Entity\Customer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->redirect($this->generateUrl('address'));
    }

    /**
     * @Route("/address", name="address")
     */
    public function addressAction(Request $request)
    {
        $addresses = $this->getDoctrine()
            ->getRepository(Address::class)
            ->findAll();

        return $this->render('AppBundle:Address:index.html.twig', compact('addresses'));
    }

    /**
     * @Route("/address/create", name="create-address")
     */
    public function createAddressAction(Request $request)
    {
        $address = new Address;

        $form = $this->createFormBuilder($address)
            ->add('address', TextType::class)
            ->add('city', TextType::class)
            ->add('zipcode', TextType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($address);
            $em->flush();

            return $this->redirect($this->generateUrl('address'));
        }

        return $this->render('AppBundle:Address:form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/address/delete/{id}", name="delete-address", requirements={"id": "\d+"})
     * @Method({"POST"})
     */
    public function deleteAddressAction(Request $request)
    {

    }

    /**
     * @Route("/company", name="company")
     */
    public function companyAction(Request $request)
    {
        return $this->render('AppBundle:Company:index.html.twig', [

        ]);
    }

    /**
     * @Route("/customer", name="customer")
     */
    public function customerAction(Request $request)
    {
        return $this->render('AppBundle:Customer:index.html.twig');
    }

    /**
     * @Route("/customer/create", name="create-customer")
     */
    public function createCustomerAction(Request $request)
    {
        $customer = new Customer;
        $addresses = $this->getDoctrine()->getRepository(Address::class)->findAll();
        $addr = [];
        foreach ($addresses as $address){
            $addr[$address->getAddress()] = $address->getId();
        }

        $form = $this->createFormBuilder($customer)
            ->add('name', TextType::class)
            ->add('age', TextType::class)
            ->add('email', TextType::class)
            ->add('billing', ChoiceType::class, array(
                'placeholder' => 'Choose an option',
                'choices'  => $addr
            ))
            ->add('shipping', ChoiceType::class, array(
                'placeholder' => 'Choose an option',
                'choices'  => $addr
            ))
            ->add('save', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $form->getData();

            $customer->setBilling($this->getDoctrine()->getRepository(Address::class)->find($customer->getBilling()));
            $customer->setShipping($this->getDoctrine()->getRepository(Address::class)->find($customer->getShipping()));
            //dump($customer);exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();

            return $this->redirect($this->generateUrl('customers-in-twig'));
        }

        return $this->render('AppBundle:Customer:form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/customers-in-json", name="customers-in-json")
     * @Method({"GET"})
     */
    public function actionCustomersJsonAction() {
        return $this->render('AppBundle:Customer:index-json.html.twig');
    }

    /**
     * @Route("/get-all-customers-in-json", name="get-all-customers-in-json")
     * @Method({"GET"})
     */
    public function actionGetAllCustomersInJson() {
        $customers = $this->getDoctrine()
            ->getRepository(Customer::class)
            ->findAll();

        $result = ['success' => true];
        if ($customers) {
            $serializer = new Serializer(
                [
                    new GetSetMethodNormalizer()
                ],
                [
                    'json' => new JsonEncoder()
                ]
            );

            $response = $serializer->serialize($customers, 'json');

            $result['success'] = true;
            $result['data'] = json_decode($response);
        }

        return new JsonResponse($result);

    }

    /**
     * @Route("/customers-in-twig", name="customers-in-twig")
     * @Method({"GET"})
     */
    public function actionCustomersInTwigAction() {
        $customers = $this->getAllCustomersInTwig();

        return $this->render('AppBundle:Customer:index.html.twig', compact('customers'));
    }

    /**
     * @return array
     */
    private function getAllCustomersInTwig() {
        $customers = $this->getDoctrine()
            ->getRepository(Customer::class)
            ->findAll();

        if (!$customers) {
            throw new NotFoundHttpException('Customers not found.');
        }

        return $customers;
    }

    /**
     * @Route("/orders", name="orders")
     */
    public function ordersAction(Request $request)
    {
        return $this->render('AppBundle:Orders:index.html.twig', [

        ]);
    }
}
